FROM alpine

WORKDIR /home

ADD . ./

RUN apk --no-cache add curl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN chmod +x ./kubectl_init.sh
RUN mv ./kubectl /usr/local/bin/kubectl