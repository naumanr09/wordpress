#!/bin/sh

if  type "kubectl" > /dev/null ; 
then
    echo "kubectl init..."
    kubectl config set-cluster testa --server="$KUBE_URL" --insecure-skip-tls-verify=true 
    kubectl config set-credentials admin --username="$KUBE_USER" --password="$KUBE_PASSWORD"
    kubectl config set-context default --cluster=testa --user=admin
    if [ -z "$ENVIRONMENT" ]; then NAMESPACE=wordpress-dev; else NAMESPACE=wordpress-$ENVIRONMENT; fi
    kubectl config set-context default --namespace=$NAMESPACE
    kubectl config use-context default
fi